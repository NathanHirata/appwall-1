"use scrict";

Template.posts.helpers({
            postList: function () {
                return Collections.Tasks.find({}, {
                    sort: {
                        createdAt: -1
                    }
                });
            },

            updateMasonry: function () {
                $('.grid').imagesLoaded().done(function () {
                    $('.grid').masonry({
                        itemSelector: '.grid-item',
                        gutter: 10,
                        columnWidth: '.grid-sizer',
                        percentPosition: true
                    });
                });
            },


            imageIs: function (ImageId) {
                var image = Collections.Images.find({
                    _id: ImageId
                }).fetch();
                return image[0].url();
            }
});