"use strict";

Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postImage = event.currentTarget.children[0].files[0];

        var postName = event.currentTarget.children[3].value;

        var postMessage = event.currentTarget.children[5].value;

        if (postName === "") {
            Materialize.toast('Nothing is being submitted', 4000)
        }

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                Materialize.toast('error', 4000);
                return false;
            } else {
                Materialize.toast('Success', 4000);
                Collections.Tasks.insert({
                    name: postName,
                    createdAt: new Date(),
                    message: postMessage,
                    imageId: fileObject._id
                });
            }
        });
    }
});

Accounts.ui.config({
passwordSignupFields: "USERNAME_ONLY"
});